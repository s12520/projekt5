package service;

import static org.junit.Assert.*;

import java.awt.List;
import java.util.ArrayList;

import org.junit.Test;

public class UserServiceTest {

    @Test(expected=NullPointerException.class)
    public void col_services_should_throw_exception_if_null_is_entered_as_a_parameter(){
            UserService.findUsersWhoHaveMoreThanOneAddress(null);
    }

    @Test
    public void col_services_should_return_empty_list_if_there_is_no_result(){
           
            User u = new User();
            Person p = new Person();
            u.setPersonDetails(p);
            List<User> users = new ArrayList<User>();
            users.add(u);
           
            List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);
           
            assertTrue(result.size()==0);
    }
   
    @Test
    public void col_services_should_ignore_users_without_person_details(){
            User u = new User();
            User userWith2Addresses = new User();
            Person p = new Person();
            Person p2 = new Person();
            Address a1 = new Address();
            Address a2 = new Address();
            Address a3 = new Address();
            p.getAddresses().add(a1);
            p2.getAddresses().add(a3);
            p2.getAddresses().add(a2);
            u.setPersonDetails(p);
            User userWithoutPersonDetails = new User();
            userWith2Addresses.setPersonDetails(p2);

            List<User> users = new ArrayList<User>();
            users.add(u);
            users.add(userWith2Addresses);
            users.add(userWithoutPersonDetails);

            List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);

            assertTrue(result.size()==1);
            assertSame(result.get(0), userWith2Addresses);
    }
   
    @Test(expected=NullPointerException.class)
    public void exception_if_null_is_entered_as_a_parameter(){
            UserService.findOldestPerson(null);
    }

    @Test
    public void empty_list_of_names_if_there_is_no_result(){
           
            User u = new User();
            Person p = new Person();
            u.setPersonDetails(p);
            List<User> users = new ArrayList<User>();
            users.add(u);
           
            <User> result = UserService.findOldestPerson(users);
           
            assertTrue(result.size()==0);
    }
	  
    
    @Test
    public void col_services_should_return_proper_result_if_there_exists_user_with_more_than_1_address()
    {

            User user = new User();
            User userWith2Addresses = new User();
            Person per1 = new Person();
            Person per2 = new Person();
            Address a1 = new Address();
            Address a2 = new Address();
            Address a3 = new Address();
            per1.getAddresses().add(a1);
            per2.getAddresses().add(a3);
            per2.getAddresses().add(a2);
            user.setPersonDetails(per1);
            userWith2Addresses.setPersonDetails(per2);
           
            List<User> users = new ArrayList<User>();
            users.add(user);
            users.add(userWith2Addresses);

            List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);
           
            assertTrue(result.size()==1);
            assertSame(result.get(0), userWith2Addresses);
    }
    
    @Test
	public void proper_result_if_longest_username_is_found()
	{
		
		List<String> EdwardsPhoneNumber = new ArrayList<String>();
		EdwardsPhoneNumber.add("622454323");
		List<Address> EdwardsAddresses = new ArrayList<Address>();
		EdwardsAddresses.add(new Address("Plac Wiktorii", 22, 1, "Szczecin", "82-300", "Poland"));
		List<Permission> permissions = new ArrayList<Permission>();
		permissions.add(new Permission("Drzewo"));
		Role EdwardsRole = new Role("gardener", permissions);
		Person user1Details = new Person("Edward", "Kallen", EdwardsPhoneNumber, EdwardsAddresses, joesRole, 16);
		User user1 = new User("Drakula", "ksiezycz", user1Details);
		
		
		List<String> WiktorsPhoneNumber = new ArrayList<String>();
		WiktorsPhoneNumber.add("111222333");
		List<Address> WiktorsAddresses = new ArrayList<Address>();
		WiktorsAddresses.add(new Address("Aleja Popiela", 1, 15, "Bydgoszcz", "82-301", "Poland"));
		List<Permission> Wiktorspermissions = new ArrayList<Permission>();
		Wiktorspermissions.add(new Permission("to visit"));
		Role WiktorsRole = new Role("Wiktor", permissions);
		Person user2Details = new Person("Wiktor", "Korolkiewicz", WiktorsPhoneNumber, WiktorsAddresses, WiktorsRole, 26);
		User user2 = new User("Dziadek", "Tred", user2Details);
		
		
		List<String> WiktoriasPhoneNumbers = new ArrayList<String>();
		WiktoriasPhoneNumbers.add("321654987");
		List<Address> WiktoriasAddresses = new ArrayList<Address>();
		WiktoriasAddresses.add(new Address("fairy", 32, 15, "Olsztyn", "10-343", "Poland"));
		List<Permission> Wiktoriaspermissions = new ArrayList<Permission>();
		Wiktoriaspermissions.add(new Permission("cry"));
		Role wojteksRole = new Role("secret", permissions);
		Person user3Details = new Person("Wiktoria", "Poo", WiktoriasPhoneNumbers, WiktoriasAddresses, wojteksRole, 2);
		User user3 = new User("Loli", "got", user3Details);
		
		assertEquals(user1, UserService.findUserWithLongestUsername(Arrays.asList(
				user2,
				user3
		)));
	}
	
	@Test
	public void proper_result_if_people_above_18_found()
	{
		
		
		List<String> EdwardsPhoneNumber = new ArrayList<String>();
		EdwardsPhoneNumber.add("622454323");
		List<Address> EdwardsAddresses = new ArrayList<Address>();
		EdwardsAddresses.add(new Address("Plac Wiktorii", 22, 1, "Szczecin", "82-300", "Poland"));
		List<Permission> permissions = new ArrayList<Permission>();
		permissions.add(new Permission("Drzewo"));
		Role EdwardsRole = new Role("gardener", permissions);
		Person Edward = new Person("Edward", "Kallen", EdwardsPhoneNumber, EdwardsAddresses, joesRole, 16);
		
		
		List<String> WiktorsPhoneNumber = new ArrayList<String>();
		WiktorsPhoneNumber.add("111222333");
		List<Address> WiktorsAddresses = new ArrayList<Address>();
		WiktorsAddresses.add(new Address("Aleja Popiela", 1, 15, "Bydgoszcz", "82-301", "Poland"));
		List<Permission> Wiktorspermissions = new ArrayList<Permission>();
		Wiktorspermissions.add(new Permission("to visit"));
		Role WiktorsRole = new Role("Wiktor", permissions);
		Person Wiktor = new Person("Wiktor", "Korolkiewicz", WiktorsPhoneNumber, WiktorsAddresses, WiktorsRole, 26);
		
		
		List<String> WiktoriasPhoneNumbers = new ArrayList<String>();
		WiktoriasPhoneNumbers.add("321654987");
		List<Address> WiktoriasAddresses = new ArrayList<Address>();
		WiktoriasAddresses.add(new Address("fairy", 32, 15, "Olsztyn", "10-343", "Poland"));
		List<Permission> Wiktoriaspermissions = new ArrayList<Permission>();
		Wiktoriaspermissions.add(new Permission("cry"));
		Role wojteksRole = new Role("secret", permissions);
		Person Wiktoria = new Person("Wiktoria", "Poo", WiktoriasPhoneNumbers, WiktoriasAddresses, wojteksRole, 2);
		
		List<String> DanielsPhoneNumbers = new ArrayList<String>();
		DanielsPhoneNumbers.add("656546565");
		List<Address> DanielsAddresses = new ArrayList<Address>();
		wojteksAddresses.add(new Address("Drzewna", 2, 2, "Dabrowa", "10-666", "Poland"));
		List<Permission> Danielspermissions = new ArrayList<Permission>();
		Danielspermissions.add(new Permission("to saw"));
		Role DanielsRole = new Role("Zszywacz", permissions);
		Person Daniel = new Person("Daniel", "Dratwa", DanielsPhoneNumbers, DanielsAddresses, DanielsRole, 33);
		
		
		
		assertEquals("Wiktor Korolkiewicz, Daniel Dratwa", UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(Arrays.asList(
				Edward,
				Wiktor,
				Wiktoria,
				Daniel
		)));
	}
	
	@Test
	public void proper_result_if_people_with_names_starting_with_A_found()
	{
		
		List<String> AdamsPhoneNumbers = new ArrayList<String>();
		AdamsPhoneNumbers.add("4355252345");
		List<Address> AdamsAddresses = new ArrayList<Address>();
		AdamsAddresses.add(new Address("Nowowiejska", 2, 5, "Gdynia", "81-231", "Poland"));
		List<Permission> permissions = new ArrayList<Permission>();
		permissions.add(new Permission("to tie"));
		Role AdamsRole = new Role("soleninzant", permissions);
		Person Adam = new Person("Adam", "Grubovsky", AdamsPhoneNumbers, AdamsAddresses, AdamsRole, 16);
		
		List<String> WiktorsPhoneNumber = new ArrayList<String>();
		WiktorsPhoneNumber.add("111222333");
		List<Address> WiktorsAddresses = new ArrayList<Address>();
		WiktorsAddresses.add(new Address("Aleja Popiela", 1, 15, "Bydgoszcz", "82-301", "Poland"));
		List<Permission> Wiktorspermissions = new ArrayList<Permission>();
		Wiktorspermissions.add(new Permission("to visit"));
		Role WiktorsRole = new Role("Wiktor", permissions);
		Person Wiktor = new Person("Wiktor", "Korolkiewicz", WiktorsPhoneNumber, WiktorsAddresses, WiktorsRole, 26);
		
		
		List<String> AlexPhoneNumbers = new ArrayList<String>();
		AlexPhoneNumbers.add("65645657");
		List<Address> AlexAddresses = new ArrayList<Address>();
		AlexAddresses.add(new Address("Balladyny", 3, 21, "Legnica", "10-343", "Poland"));
		List<Permission> Alexpermissions = new ArrayList<Permission>();
		Alexpermissions.add(new Permission("cry"));
		Role AlexRole = new Role("muzykant", permissions);
		Person Alex = new Person("Alex", "Kirszew", AlexPhoneNumbers, AlexAddresses, AlexRole, 2);
		
		List<String> DanielsPhoneNumbers = new ArrayList<String>();
		DanielsPhoneNumbers.add("656546565");
		List<Address> DanielsAddresses = new ArrayList<Address>();
		wojteksAddresses.add(new Address("Drzewna", 2, 2, "Dabrowa", "10-666", "Poland"));
		List<Permission> Danielspermissions = new ArrayList<Permission>();
		Danielspermissions.add(new Permission("to saw"));
		Role DanielsRole = new Role("Zszywacz", permissions);
		Person Daniel = new Person("Daniel", "Dratwa", DanielsPhoneNumbers, DanielsAddresses, DanielsRole, 33);
		
		assertEquals(Arrays.asList(
				"soleninzant",
				"muzykant"
		), UserService.getSortedPermissionsOfUsersWithNameStartingWithA(Arrays.asList(
				Adam,
				Wiktor,
				Alex,
				Daniel
		)));
	}
    
	@Test
	public void proper_result_of_grouping_by_age_above_18_or_less()
	{
		
		List<String> AdamsPhoneNumbers = new ArrayList<String>();
		AdamsPhoneNumbers.add("4355252345");
		List<Address> AdamsAddresses = new ArrayList<Address>();
		AdamsAddresses.add(new Address("Nowowiejska", 2, 5, "Gdynia", "81-231", "Poland"));
		List<Permission> permissions = new ArrayList<Permission>();
		permissions.add(new Permission("to tie"));
		Role AdamsRole = new Role("soleninzant", permissions);
		Person Adam = new Person("Adam", "Grubovsky", AdamsPhoneNumbers, AdamsAddresses, AdamsRole, 16);
		
		List<String> WiktorsPhoneNumber = new ArrayList<String>();
		WiktorsPhoneNumber.add("111222333");
		List<Address> WiktorsAddresses = new ArrayList<Address>();
		WiktorsAddresses.add(new Address("Aleja Popiela", 1, 15, "Bydgoszcz", "82-301", "Poland"));
		List<Permission> Wiktorspermissions = new ArrayList<Permission>();
		Wiktorspermissions.add(new Permission("to visit"));
		Role WiktorsRole = new Role("Wiktor", permissions);
		Person Wiktor = new Person("Wiktor", "Korolkiewicz", WiktorsPhoneNumber, WiktorsAddresses, WiktorsRole, 26);
		
		
		List<String> AlexPhoneNumbers = new ArrayList<String>();
		AlexPhoneNumbers.add("65645657");
		List<Address> AlexAddresses = new ArrayList<Address>();
		AlexAddresses.add(new Address("Balladyny", 3, 21, "Legnica", "10-343", "Poland"));
		List<Permission> Alexpermissions = new ArrayList<Permission>();
		Alexpermissions.add(new Permission("cry"));
		Role AlexRole = new Role("muzykant", permissions);
		Person Alex = new Person("Alex", "Kirszew", AlexPhoneNumbers, AlexAddresses, AlexRole, 2);
		
		List<String> DanielsPhoneNumbers = new ArrayList<String>();
		DanielsPhoneNumbers.add("656546565");
		List<Address> DanielsAddresses = new ArrayList<Address>();
		wojteksAddresses.add(new Address("Drzewna", 2, 2, "Dabrowa", "10-666", "Poland"));
		List<Permission> Danielspermissions = new ArrayList<Permission>();
		Danielspermissions.add(new Permission("to saw"));
		Role DanielsRole = new Role("Zszywacz", permissions);
		Person Daniel = new Person("Daniel", "Dratwa", DanielsPhoneNumbers, DanielsAddresses, DanielsRole, 33);
		
		Map<Boolean, List<User>> map = new Map();
		map.put(true, Arrays.asList(Wiktor, Daniel));
		map.put(false, Arrays.asList(Adam, Alex));
		
		assertEquals(map, UserService.partitionUserByUnderAndOver18(Arrays.asList(
				Adam,
				Wiktor,
				Alex,
				Daniel
		)));
}
	
	
	@Test
	public void test() {
		fail("Not yet implemented");
	}

}