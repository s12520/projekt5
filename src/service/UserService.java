	    package service;
     
    import domain.Person;
    import domain.Role;
    import domain.User;
     
    import java.util.Comparator;
    import java.util.List;
    import java.util.Map;
    import java.util.stream.Collectors;
     
     
    public class UserService {
           
     
        public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
           
            return (List<User>) ((Object) users).stream()
     
            .filter(user -> user.getPersonDetails().getAddresses().size() > 1);
                           
                    }
     
        public static User findOldestPerson(List<User> users) {
           
                           
                                    Comparator<User> comp = (user1, user2) -> Integer.compare( user1.getPersonDetails().getAge(),user2.getPersonDetails().getAge());
                                User oldest = users.stream()
                                                          .max(comp)
                                                          .get();
     
        public static User findUserWithLongestUsername(List<User> users) {
           
            return users.stream()
                            .max(
                                            Comparator<User> comparison = (user1, user2) -> Integer.compare(user1.getName().length(),user2.getName().length());
                                           
                            .get();
           
           
        }
     
        public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
           
            return users.stream()
                              .filter(user -> user.getPersonDetails().getAge() > 18)
                              .map(user -> user.getPersonDetails().getName() + " " + user.getPersonDetails().getSurname()  )
                              .collect(Collectors.joining(" , "));
           
                   
        }
     
        public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
           
            return users.stream()
            .filter( p-> user.getName().charAt(0) == 'A'
                    .sorted((permission1, permission2) -> permission1.compareTo(permission2)
            .collect(Collectors.toList());
               
       }
     
        public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
           
            users.stream()
                    .filter(user-> user.getPersonDetails().getSurname().charAt(0) == 's')
                    .map(user -> user.getPersonDetails().getRole().getPermission().getName().toUpperCase())
                    .forEach(System.out::println);
      }
     
      public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
                       
             return users.stream()
                                   
                            .collect(Collectors.groupingBy(
                                                    user -> user.getPersonDetails().getRole()
                                    ));
        }
     
      public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
           
              return users.stream()
              .collect(Collectors.partitioningBy(user -> user.getPersonDetails().getAge() >= 18));
        }
    }

